<?php

class Programmer {
    public $name = "";
    protected $lang = "";
    public $exp  = "";
    
    function __construct($name = "", $lang = "", $exp = "") {
	$this->name = $name;
	$this->lang = $lang;
	$this->exp = $exp;
    }
    
    function getName() {
	return $this->name;
    }
    
    protected function setName($name) {
	$this->name = $name;
    }
} 

class PHPprogrammer extends Programmer {
    public $phpExp = "total PHP exprience";
}

class LaravelProgrammer extends PHPprogrammer {
    public $laravelExp = "minimum years";
    
    public $lang = "test";
    
    function getName() {
	return $this->name;
    }
    
    public function setName($name) {
	echo "Setting from Laravel class <br />";
	$this->name = $name;
    }
}

$mizan = new LaravelProgrammer("mizanur rahman","PHP, JS",10);
$hasin = new PHPprogrammer("hasin hayder","PHP, Go",14);

print_r($mizan);

print_r($hasin);
$mizan->setName("mizan");
echo $mizan->lang;
