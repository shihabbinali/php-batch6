<?php

class Programmer {
    public $name = "";
    protected $lang = "";
    public $exp  = "";
    
    function __construct($name = "", $lang = "", $exp = "") {
	$this->name = $name;
	$this->lang = $lang;
	$this->exp = $exp;
    }
    
    function getName() {
	echo LaravelProgrammer::getName();
	return $this->name;
    }
    
    public function setName($name) {
	$this->name = $name;
    }
} 

class PHPprogrammer extends Programmer {
    public $phpExp = "total PHP exprience";
    
    public function __construct($name = "", $lang = "", $exp = "") {
	parent::__construct($name, $lang , $exp);
    }
}

class LaravelProgrammer extends PHPprogrammer {
    public $laravelExp = "";
    public $laravelVersion = "";
    
    public $lang = "test";
    
    function __construct($name = "", $lang = "", $exp = "", $lExp = "", $version = "") {	
	Programmer::__construct($name, $lang , $exp);
	$this->laravelExp = $lExp;
	$this->laravelVersion = $version;
    }
    
    function getName() {
	echo "Calling laravel programmer get name";
	//return $this->name;
    }
    
    public function setName($name) {
	echo "Setting from Laravel class <br />";
	$this->name = $name;
    }
}

$mizan = new LaravelProgrammer("mizanur rahman","PHP, JS",10, 3, "5.3");
$hasin = new Programmer("hasin hayder","PHP, Go",14);

print_r($mizan);

print_r($hasin);
$mizan->setName("mizan");
echo $mizan->lang;
echo $hasin->getName();