<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Programmer {
    public static function testName() {
	echo static::showName();
    }
    
    public static function showName() {
	return "Programmer";
    }
}

class PHPprogrammer extends Programmer{
    
    public static function showName() {
	return "PHP Programmer";
    }
}

echo PHPprogrammer::testName()."<br />";
echo Programmer::testName()."<br />";

