<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

interface Printer {
    
    public function showPrinterName();
    
    public function doPrint();
    
}

interface Security {
    
    public function showSecurity();
}

class Something implements Printer, Security {
    
    public function showPrinterName() {
	echo "HP color printer";
    }
    
    public function doPrint() {
	echo "I am printing";
    }
    
    public function showSecurity() {
	
    }
    
}