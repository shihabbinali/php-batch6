<?php

interface  printing {
    public function doPrint();
}

trait BasicProgrammer {
    
    public $expertise;
    
    function showIamProgrammer() {
	echo "I am programmer from trait <br />";
    }
    
}

trait WebProgrammer {
    
    public $expertise;
    
    function showIamProgrammer() {
	echo "I am programmer from basic trait <br />";
    }
    
}


class Programmer implements printing{
    public $name = "";
    protected $lang = "";
    public $exp  = "";
    
    use BasicProgrammer, WebProgrammer {
	BasicProgrammer::showIamProgrammer insteadof WebProgrammer;
	WebProgrammer::showIamProgrammer as AnotherShow;
    }
    
    function __construct($name = "", $lang = "", $exp = "") {
	$this->name = $name;
	$this->lang = $lang;
	$this->exp = $exp;
    }
    
    function getName() {
	return $this->name;
    }
    
    protected function setName($name) {
	$this->name = $name;
    }
    
    public function doPrint() {
	
    }
} 



class LaravelProgrammer extends Programmer {
    public $laravelExp = "minimum years";
    
    
    
    public $lang = "test";
    
    function getName() {
	return $this->name;
    }
    
    public function testName(printing $obj) {
	echo $obj->showIamProgrammer();
    }
    
    public function setName($name) {
	echo "Setting from Laravel class <br />";
	$this->name = $name;
    }
}

$mizan = new Programmer("mizanur rahman","PHP, JS",10);
$mizan->showIamProgrammer();
$mizan->AnotherShow();

$hasin = new LaravelProgrammer("hasin hayder","PHP, Go",14);


$obj = new stdClass();

$hasin->testName($mizan);

