<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

interface knowledge {
    public function knows();
}

class Programmer implements knowledge{
    
    public static function knows() {
	return "Programmer";
    }
}

class PHPprogrammer implements knowledge{ 
    
    public static function knows() {
	return "PHP";
    }
}

class Laravelprogrammer implements knowledge{
    
    public static function knows() {
	return "Laravel";
    }
}

$sumon = new Laravelprogrammer;
$a = new Programmer;
$arr = [];
$arr[] = $sumon;
$arr[] = $a;

foreach($arr as $obj)
    echo $obj->knows();
