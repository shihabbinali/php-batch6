<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class Elipse {
    
    public $radius;
    
    public function __construct($radius) {
	$this->radius = $radius;
    }
    
    public function area() {
	return (22/7)*$this->radius * $this->radius;
    }
    
}
